import React, { useEffect, useState } from "react";
import axios from "axios";
import QuizItem from "./QuizItem";

const QuizList = () => {
  const [quiz, setQuiz] = useState([]);

  useEffect(() => {
    loadQuizList();
  }, []);
  const loadQuizList = async () => {
    const result = await axios.get(
      "http://localhost:8080/quiz_builderSpring/api/QuizBuilder/"
    );
    console.log(result.data);
    setQuiz(result?.data);
  };

  return (
    <div>
      <h1>Quiz List</h1>
      {quiz?.length > 0 &&
        quiz.map((quiz) => {
          return <QuizItem key={quiz?.id} props={quiz} />;
        })}
    </div>
  );
};

export default QuizList;
