import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

const QuizItem = ({ props }) => {
  const { title, desc, logo } = props;
  return (
    <div className="grid-rows-3">
      <Card>
        <Card.Img variant="top" src={logo} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{desc}</Card.Text>
          <Button variant="primary">Play Quiz</Button>
        </Card.Body>
      </Card>
    </div>
  );
};

export default QuizItem;
