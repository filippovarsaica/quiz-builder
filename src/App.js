import "./App.css";
import Homepage from "./QuizHomepage/Homepage.jsx";
import { Route, Routes } from "react-router-dom";
import QuizList from "./QuizView/QuizList";
import QuizForm from "./QuizCreation/QuizForm";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/quizcreate" element={<QuizForm />} />
        <Route path="/quizplay" element={<QuizList />} />
      </Routes>
    </div>
  );
}

export default App;
