import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { useNavigate } from "react-router";

const Homepage = () => {
  const navigate = useNavigate();

  const redirectQuizPlayPage = () => {
    navigate("/quizplay");
  };

  const redirectQuizCreatePage = () => {
    navigate("/quizcreate");
  };

  return (
    <div>
      <Container>
        <Row>
          <Col>
            <Card className="text-center">
              <Card.Body>
                <Card.Title>Play Quiz</Card.Title>
                <Button onClick={() => redirectQuizPlayPage()} variant="dark">
                  Play
                </Button>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card className="text-center">
              <Card.Body>
                <Card.Title>Create Quiz</Card.Title>
                <Button onClick={() => redirectQuizCreatePage()} variant="dark">
                  Create
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Homepage;
